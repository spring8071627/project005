package com.example.demo.Quiz;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.dto.StudentDTO;

@Controller
@RequestMapping("/return")
public class QuizController {

	@GetMapping("/q1")
	public void quiz1() {
	}
	@ResponseBody
	@GetMapping("/q4")
	public StudentDTO quiz4(){
		StudentDTO studentDTO = new StudentDTO(1,"둘리",3);
		return studentDTO;
	}
}
