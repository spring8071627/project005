package com.example.demo.Quiz;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.dto.StudentDTO;

@Controller
@RequestMapping("/quiz")
public class QuizController3 {


@GetMapping("/q1")
public ResponseEntity q1( String a){
	System.out.println("문자열 파라미터 : " + a);
	return new ResponseEntity(HttpStatus.OK);
}

@GetMapping("/q2")
public ResponseEntity q2(float f , boolean a) {
	System.out.println("실수 : " + f + "boolean : " + a);
	return new ResponseEntity(HttpStatus.OK);
	
}
@GetMapping("/q3")
public ResponseEntity q3(char[] arr) {
	for(int i=0;i<arr.length;i++) {
		System.out.println(arr[i]);
	}
	System.out.println("배열의 크기: " + arr.length);
	return new ResponseEntity(HttpStatus.OK);
	
}
@PostMapping("/q4")
public ResponseEntity q4(@RequestBody StudentDTO studentDTO) {
	System.out.println("학생객체 수집 : " + studentDTO);
	return new ResponseEntity(HttpStatus.OK);
	
}
@PostMapping("/q5")
public ResponseEntity q5(@RequestBody ArrayList<StudentDTO>list) {
	System.out.println("학생객체 리스트 수집 : " + list);
	System.out.println("리스트 개수:" + list.size());
	return new ResponseEntity(HttpStatus.OK);
}
}
