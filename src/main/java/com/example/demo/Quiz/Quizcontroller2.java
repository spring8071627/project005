package com.example.demo.Quiz;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/quiz")
public class Quizcontroller2 {
	@GetMapping("/q")
	public ResponseEntity q1() {
		System.out.println("get방식");
		return new ResponseEntity(HttpStatus.OK);
	}

	@PostMapping("/q")
	public ResponseEntity q2() {
		System.out.println("post방식");
		return new ResponseEntity(HttpStatus.OK);
	}

	@PutMapping("/q")
	public ResponseEntity q3() {
		System.out.println("put방식");
		return new ResponseEntity(HttpStatus.OK);
	}

	@DeleteMapping("/q")
	public ResponseEntity q4() {
		System.out.println("delete방식");
		return new ResponseEntity(HttpStatus.OK);
	}
}
