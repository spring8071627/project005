package com.example.demo.Sample;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.dto.BookDTO;

@Controller
@RequestMapping("/param")
public class SampleController3 {

	@GetMapping("/ex1")
	public ResponseEntity ex1(@RequestParam int i){
		System.out.println("int형 파라미터 수집 : " + i);
		return new ResponseEntity(HttpStatus.OK);
		//localhost:8080/param/ex1?i=100
	}
	
	@GetMapping("/ex2")
	public ResponseEntity ex2(int i, char c) {
		System.out.println("int형 파라미터 수집: " + i + ", char형 파라미터 수집: " + c);
		return new ResponseEntity(HttpStatus.OK); 
		//localhost:8080/param/ex2?i=100&c=a
	}
	
	@GetMapping("/ex3")
	public ResponseEntity ex3(int[] arr) {
		System.out.println("int형 배열 수집 : " + Arrays.toString(arr));
		return new ResponseEntity(HttpStatus.OK);
	}
	@GetMapping("/ex4")
	public ResponseEntity ex4(@RequestParam ArrayList<Integer> list) {
		System.out.println("int형 리스트 수집 : "+ list);
		return new ResponseEntity(HttpStatus.OK);
		//localhost:8080/param/ex4?list=1&list=2&list=3
	}
	@GetMapping("/ex5")
	public ResponseEntity ex5(@DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) {
		System.out.println("날짜 수집: " + date);
		return new ResponseEntity<>(HttpStatus.OK);
		//localhost:8080/param/ex5?date=2023-07-20
	}
	@GetMapping("/ex6/{i}")
	public ResponseEntity ex6(@PathVariable int i) {
		System.out.println("int형 파라미터 수집: " + i);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	@GetMapping("/ex7")
	public ResponseEntity ex7(@RequestBody BookDTO bookDTO) {
		System.out.println("객체수집: " + bookDTO);
		return new ResponseEntity(HttpStatus.OK);
	}
	
	@GetMapping("/ex8")
	public ResponseEntity ex8(@RequestBody ArrayList<BookDTO>list) {
		System.out.println("객체타입 리스트 수집 : " + list);
		return new ResponseEntity(HttpStatus.OK);
	}
}
