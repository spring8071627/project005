package com.example.demo.Sample;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.dto.BookDTO;

/*
 * 컨트롤러 리턴타입
 * */
@Controller
@RequestMapping("/Return")
public class SampleController {

	@GetMapping("/ex1")
	public void ex1() {
		// template/ + /return + /ex1.html 파일을 자동으로 반환
	}
	
	@GetMapping("ex2")
	public String ex2() {
		return "/Return/ex1.html"; // html경로를 지정하여 반환
	}
	
	@ResponseBody
	@GetMapping("/ex3")
	public BookDTO ex3() {
		BookDTO bookDto = new BookDTO("자바프로그래밍입문","한빛컴퍼니",20000);
		return bookDto;
	}
	
	@ResponseBody
	@GetMapping("/ex4")
	public List<BookDTO> ex4(){
		List<BookDTO> list = new ArrayList<>();
		list.add(new BookDTO("자바프로그래밍입문", "한빛컴퍼니", 20000));
		list.add(new BookDTO("스프링부트웹프로젝트", "구멍가게코딩단", 20000));
		list.add(new BookDTO("모두의리눅스", "길빛출판사", 20000));
		return list;
	}
	
	@GetMapping("/ex5")
	public ResponseEntity ex5() {
		return new ResponseEntity<>(HttpStatus.OK);
		
	}
	
	@GetMapping("/ex6")
	public ResponseEntity<String> ex6(){
		return new ResponseEntity<String>("success..",HttpStatus.OK);
	}
	
	@GetMapping("/ex7")
	public ResponseEntity<BookDTO> ex7(){
		BookDTO bookDTO = new BookDTO("자바프로그래밍입문","한빛컴퍼니",20000);
		return new ResponseEntity<>(bookDTO, HttpStatus.OK);
	}
	
	
}
